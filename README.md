# Frigeri's style cv

CVs in latex using the [Friggeri resume cv](https://www.sharelatex.com/templates/cv-or-resume/fancy-cv) LateX template.

CVs are organized in the different tex files.

## Pas Normal Studio

```
pas-normal-studios.tex
```

Resume for the [Pas Normal Studios Activation Manager position](https://www.pasnormalstudios.com/introducing-best-job-world/).
